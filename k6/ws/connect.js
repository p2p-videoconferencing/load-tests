import ws from 'k6/ws';
import { check } from "k6";

const BASE_URL = "wss://p2p-videoconferencing.ru";

let statuses = {
    "status was 20x": (r) => r.status >= 200 && r.status <= 299,
    "status was 30x": (r) => r.status >= 300 && r.status <= 399,
    "status was 40x": (r) => r.status >= 400 && r.status <= 499,
    "status was 50x": (r) => r.status >= 500 && r.status <= 599
};

export let options = {
    stages: [
        {duration: "10s", target: 10},
        {duration: "1m", target: 10},
        {duration: "10s", target: 20},
        {duration: "3m", target: 20},
        {duration: "10s", target: 30},
        {duration: "3m", target: 30},
        {duration: "10s", target: 40},
        {duration: "4m", target: 40},
        {duration: "10s", target: 50},
        {duration: "4m", target: 50},
    ],
    insecureSkipTLSVerify: true
};

export default function() {
    const url = BASE_URL + '/ws/';

    const res = ws.connect(url, null, function (socket) {
        socket.on('open', function open() {
            // ...
            console.log('WebSocket connection established!');
            socket.send("42[\"join room\",{\"roomID\":\"1cff2110-cae0-11ec-b66f-952a461f73d0\",\"username\":\"Guest\"}]")
        });

        socket.on('message', (data) => console.log('Message received: ', data));

        socket.on('error', function (e) {
            if (e.error() !== 'websocket: close sent') {
                console.log('An unexpected error occured: ', e.error());
            }
        });
    });

    check(res, { 'status is 101': (r) => r && r.status === 101 });
}