import http from "k6/http";
import { group, check, sleep } from "k6";

const BASE_URL = "https://p2p-videoconferencing.ru";

const OAUTH_TOKEN = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImZjYmQ3ZjQ4MWE4MjVkMTEzZTBkMDNkZDk0ZTYwYjY5ZmYxNjY1YTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTAwMjYxMTYyNTQzMS1wbWJjODV0dWVmM2E5c3Rzb2swNHVuZHZmNTViMW5jMS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImF1ZCI6IjEwMDI2MTE2MjU0MzEtcG1iYzg1dHVlZjNhOXN0c29rMDR1bmR2ZjU1YjFuYzEuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTEzMzMzNzYyMjUyNzcwMDA1MTAiLCJoZCI6Inhzb2xsYS5jb20iLCJlbWFpbCI6ImEubmV1c3Ryb2V2QHhzb2xsYS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6InhNbF9hQ2NfV253M093LUt3UXpHbkEiLCJuYW1lIjoiQXJzZW55IE5ldXN0cm9ldiIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHaDRnUTB3YktLcUFrV0xYVTNhQ0J4d29XYU00ZUp6azZPck10TXI9czk2LWMiLCJnaXZlbl9uYW1lIjoiQXJzZW55IiwiZmFtaWx5X25hbWUiOiJOZXVzdHJvZXYiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTY1MTU5MTAzMywiZXhwIjoxNjUxNTk0NjMzLCJqdGkiOiI0ZmJkMTJjMWM2ZGUwMzNjNTZiZTAxYWEyMDFlZGMwMGEwOWEyY2JlIn0.LvfsaXZUD6ApEJ4sCjSkzT2nBCxufUNyPBr4MOTmHqmHKUrbedOGUBqXEBiXGW2CtFVAsIKV5qczyQG7Y812e0QiTChBvBuUA9EkPHq8cz98PDj48mf8eN4cJ0trtLtkGUFB_XgN2T6Zry7gnkTHDGm3MqwYfQX0YJe8DrSU-zuLP10NFgntiz49Tob8hAhZFj2u6ciNT9G4o48-53HnophtJvl0vhphqmMyvSPV1O5mDUUmrTcIgaw4kT_1K55PFeE3nW2ZXs3C23ldwzaxb2cJSFbPDZZdcrUHEvH06kD7WpJSmJwM3WsQQ6i9AcHo-EamYw0UQ25YDDRFs8ptbg"

const SLEEP_DURATION = 0.5;

let statuses = {
    "status was 20x": (r) => r.status >= 200 && r.status <= 299,
    "status was 30x": (r) => r.status >= 300 && r.status <= 399,
    "status was 40x": (r) => r.status >= 400 && r.status <= 499,
    "status was 50x": (r) => r.status >= 500 && r.status <= 599
};

export let options = {
    stages: [
        {duration: "10s", target: 30},
        {duration: "1m", target: 30},
        {duration: "10s", target: 50},
        {duration: "3m", target: 50},
        {duration: "10s", target: 100},
        {duration: "3m", target: 100},
        {duration: "10s", target: 150},
        {duration: "4m", target: 150},
        {duration: "10s", target: 200},
        {duration: "4m", target: 200},
        {duration: "10s", target: 100},
        {duration: "4m", target: 100},
    ],
    insecureSkipTLSVerify: true
};

export default function() {
    group("login", () => {
        // login
        let url = BASE_URL + `/api/auth/login`;
        let body = JSON.stringify({"token": OAUTH_TOKEN})
        let request = http.post(url, body, {
            headers: { 'Content-Type': 'application/json' },
        });
        check(request, statuses);
        if (request.status > 399)
        {
            console.log(request.status);
            console.log(JSON.stringify(request.body));
        }
        sleep(SLEEP_DURATION);
    });
}