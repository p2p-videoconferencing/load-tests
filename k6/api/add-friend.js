import http from "k6/http";
import { group, check, sleep } from "k6";

const BASE_URL = "https://p2p-videoconferencing.ru";

const SLEEP_DURATION = 0.5;

let statuses = {
    "status was 20x": (r) => r.status >= 200 && r.status <= 299,
    "status was 30x": (r) => r.status >= 300 && r.status <= 399,
    "status was 40x": (r) => r.status >= 400 && r.status <= 499,
    "status was 50x": (r) => r.status >= 500 && r.status <= 599
};

export let options = {
    stages: [
        {duration: "10s", target: 30},
        {duration: "1m", target: 30},
        {duration: "10s", target: 50},
        {duration: "3m", target: 50},
        {duration: "10s", target: 100},
        {duration: "3m", target: 100},
        {duration: "10s", target: 150},
        {duration: "4m", target: 150},
        {duration: "10s", target: 200},
        {duration: "4m", target: 200},
        {duration: "10s", target: 100},
        {duration: "4m", target: 100},
    ],
    insecureSkipTLSVerify: true
};

export default function() {
    group("add friend", () => {
        // login
        let url = BASE_URL + `/api/user/add-friend`;
        let body = JSON.stringify({"email": 'neustroev.arseny@gmail.com'})
        let request = http.post(url, body, {
            headers: {
                'Content-Type': 'application/json',
                'cookie': 'G_ENABLED_IDPS=google; G_AUTHUSER_H=0; connect.sid=s%3A03-rN-wtd4vBROh2jxyGfZvMJW5UF7oW.wDCWR1WoB4e8Rf9hVLjM6xSCpUywJqIIqjw0uJxYfSQ',
            },
        });
        check(request, statuses);
        if (request.status > 400)
        {
            console.log(request.status);
            console.log(JSON.stringify(request.body));
        }
        sleep(SLEEP_DURATION);
    });
}